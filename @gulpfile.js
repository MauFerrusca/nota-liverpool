/*requirements*/
var gulp  = require('gulp');
var autoprefixer  = require('gulp-autoprefixer');
var less  = require('gulp-less');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;
var wiredep = require('wiredep').stream;

/*gulp tasks*/
	/*less*/
gulp.task('less',  function(){
	return gulp
	.src('css/*.less')
	.pipe(less())
	.pipe(gulp.dest('./build/css/'))
	.pipe(browserSync.stream());
});

/*prefix*/
gulp.task('autoprefix', ['less'], function(){
	return gulp
	.src('./build/css/layout.css')
	.pipe(autoprefixer({
		browsers: ['last 2 version']
		
	}))
	.pipe(gulp.dest('./build/css/'));
});
 
gulp.task('wiredep', function () {
	  return gulp
		.src('**.html')
    .pipe(wiredep())
    .pipe(gulp.dest('./build/'));
});

/*bsync*/
gulp.task('server', function(){
	browserSync.init({
		server: './'
	});
});

/*watch*/
gulp.task('default', ['server'], function(){
	gulp.watch('./css/*.less', ['autoprefix']);
	gulp.watch('*.html').on('change', reload);
});